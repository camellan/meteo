Name:           meteo
Version:        0.9.6
Release:        1%{?dist}
Summary:        Current weather information, OpenWeather client

License:        GPLv3+
URL:            https://gitlab.com/bitseater/meteo
Source0:        https://gitlab.com/bitseater/meteo/-/archive/0.9.6/meteo-0.9.6.tar.gz

%define DEPENS_RPM rpmdevtools, git, gcc, wget, meson, ninja-build, vala, vala-devel, gtk3-devel, libsoup-devel, json-glib-devel, geocode-glib-devel, webkit2gtk3-devel, libappindicator-gtk3-devel, libappstream-glib

BuildRequires:  %{DEPENS_RPM}

%description
Current weather, with information about temperature, pressure, wind speed and direction, sunrise & sunset.

%prep
%autosetup

%build
%meson
%meson_build

%install
%meson_install

%check
%meson_test

%files
%license COPYING
%doc AUTHORS CREDITS.md README.md
%{_bindir}/com.gitlab.bitseater.meteo
%{_prefix}/lib/debug/usr/bin/com.gitlab.bitseater.meteo-0.9.6-1.fc29.x86_64.debug
%{_datadir}/applications/com.gitlab.bitseater.meteo.desktop
%{_datadir}/glib-2.0/schemas/com.gitlab.bitseater.meteo.gschema.xml
%{_datadir}/icons/hicolor/128x128/apps/com.gitlab.bitseater.meteo.svg
%{_datadir}/icons/hicolor/128x128@2/apps/com.gitlab.bitseater.meteo.svg
%{_datadir}/icons/hicolor/16x16/apps/com.gitlab.bitseater.meteo.svg
%{_datadir}/icons/hicolor/16x16@2/apps/com.gitlab.bitseater.meteo.svg
%{_datadir}/icons/hicolor/192x192/apps/com.gitlab.bitseater.meteo.svg
%{_datadir}/icons/hicolor/192x192@2/apps/com.gitlab.bitseater.meteo.svg
%{_datadir}/icons/hicolor/24x24/apps/com.gitlab.bitseater.meteo.svg
%{_datadir}/icons/hicolor/24x24@2/apps/com.gitlab.bitseater.meteo.svg
%{_datadir}/icons/hicolor/256x256/apps/com.gitlab.bitseater.meteo.svg
%{_datadir}/icons/hicolor/256x256@2/apps/com.gitlab.bitseater.meteo.svg
%{_datadir}/icons/hicolor/32x32/apps/com.gitlab.bitseater.meteo.svg
%{_datadir}/icons/hicolor/32x32@2/apps/com.gitlab.bitseater.meteo.svg
%{_datadir}/icons/hicolor/48x48/apps/com.gitlab.bitseater.meteo.svg
%{_datadir}/icons/hicolor/48x48@2/apps/com.gitlab.bitseater.meteo.svg
%{_datadir}/icons/hicolor/64x64/apps/com.gitlab.bitseater.meteo.svg
%{_datadir}/icons/hicolor/64x64@2/apps/com.gitlab.bitseater.meteo.svg
%{_datadir}/icons/hicolor/scalable/apps/com.gitlab.bitseater.meteo.svg
%{_datadir}/locale/ca/LC_MESSAGES/com.gitlab.bitseater.meteo.mo
%{_datadir}/locale/de/LC_MESSAGES/com.gitlab.bitseater.meteo.mo
%{_datadir}/locale/en/LC_MESSAGES/com.gitlab.bitseater.meteo.mo
%{_datadir}/locale/es/LC_MESSAGES/com.gitlab.bitseater.meteo.mo
%{_datadir}/locale/es_ES/LC_MESSAGES/com.gitlab.bitseater.meteo.mo
%{_datadir}/locale/fr/LC_MESSAGES/com.gitlab.bitseater.meteo.mo
%{_datadir}/locale/lt/LC_MESSAGES/com.gitlab.bitseater.meteo.mo
%{_datadir}/locale/nb/LC_MESSAGES/com.gitlab.bitseater.meteo.mo
%{_datadir}/locale/pt/LC_MESSAGES/com.gitlab.bitseater.meteo.mo
%{_datadir}/locale/pt_BR/LC_MESSAGES/com.gitlab.bitseater.meteo.mo
%{_datadir}/locale/pt_PT/LC_MESSAGES/com.gitlab.bitseater.meteo.mo
%{_datadir}/locale/ru/LC_MESSAGES/com.gitlab.bitseater.meteo.mo
%{_datadir}/locale/sr/LC_MESSAGES/com.gitlab.bitseater.meteo.mo
%{_datadir}/metainfo/com.gitlab.bitseater.meteo.appdata.xml


%changelog
* Mon Feb 11 2019 Carlos Suárez (bitseater)
- Some new improvement with units convertions
- Added Norwegian Bokmål (.nb) translation.
- Fixed issues: #116.
